package se331.lab.rest.repository;

import org.springframework.data.repository.CrudRepository;
import se331.lab.rest.entity.Activity;

import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity,Long> {

    List<Activity> findAll();
}
