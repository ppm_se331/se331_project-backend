package se331.lab.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.lab.rest.dto.*;
import se331.lab.rest.entity.*;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper( MapperUtil.class );

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);
    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);

    @Mappings({})
    TeacherDto getLecturerDto(Teacher teacher);
    @Mappings({})
    List<TeacherDto> getLecturerDto(List<Teacher> teacher);

    @Mappings({})
    StudentDto getStudentDto(Student student);
    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    CourseDto getCourseDto(Course course);
    @Mappings({})
    List<CourseDto> getCourseDto(List<Course> courses);

    @Mappings({})
    AdminDto getAdminDto(Admin admin);
    @Mappings({})
    List<AdminDto> getAdminDto(List<Admin> admins);

    @Mappings({})
    CommentDto getCommentDto(Comment comment);
    @Mappings({})
    List<CommentDto> getCommentDto(List<Comment> comment);

    @Mappings({@Mapping(target = "authorities", source = "user.authorities")
    })
    UserDto getUserDto(Student student);
    @Mappings({@Mapping(target = "authorities", source = "user.authorities")})
    UserDto getUserDto(Teacher teacher);
    @Mappings({@Mapping(target = "authorities", source = "user.authorities")})
    UserDto getUserDto(Admin admin);

}
