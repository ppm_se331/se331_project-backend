package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CommentService;

@Controller
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping("/comment")
    public ResponseEntity getAllComments(){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.getAllComments()));
    }
    @PostMapping("/comment")
    public ResponseEntity saveComment(@RequestBody Comment comment){
        return  ResponseEntity.ok(commentService.saveComment(comment));
    }
}
