package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.AdminService;

@Controller
public class AdminController {
    @Autowired
    AdminService adminService;
    @GetMapping("/admin")
    public ResponseEntity getAdmin(){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getAdminDto(adminService.getAdmin()));
    }


}
