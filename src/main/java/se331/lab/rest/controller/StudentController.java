package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.StudentService;

import java.util.HashMap;
import java.util.Map;

@Controller
@Slf4j
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        log.info("the controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllStudent()));
    }

    @GetMapping("/students/waiting")
    public ResponseEntity getAllWaitingStudents(){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getWaitingStudents()));
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.findById(id)));
    }

    @GetMapping("/students/{id}/activity")
    public ResponseEntity getStudentActivitiesById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(studentService.getStudentActivities(id)));
    }

    @PostMapping("students/approve")
    public ResponseEntity approveStudent(@RequestBody Long id){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.approveStudent(id)));
    }

    @PostMapping("students/reject")
    public ResponseEntity rejectStudent(@RequestBody Long id){
        studentService.rejectStudent(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PostMapping(path = "students")
    public ResponseEntity saveStudent(@RequestBody Student student) {
        log.info("student controller is called");
        Map result = new HashMap();
        Student returnedStudent = studentService.saveStudent(student);
        result.put("user", MapperUtil.INSTANCE.getUserDto(returnedStudent));
        result.put("student", MapperUtil.INSTANCE.getStudentDto(returnedStudent));
        return ResponseEntity.ok(result);
    }
}
