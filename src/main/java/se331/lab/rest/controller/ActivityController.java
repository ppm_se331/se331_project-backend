package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.ActivityService;

@Controller
@Slf4j
public class ActivityController {
    @Autowired
    ActivityService activityService;
    @GetMapping("/activities")
    public ResponseEntity getAllActivities() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivities()));
    }

    @GetMapping("/activities/{id}")
    public ResponseEntity getActivityById(@PathVariable("id") Long id){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.findById(id)));
    }

    @GetMapping("/activities/{id}/student")
    public ResponseEntity getActivitiesPendingStudentsById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(activityService.getActivityPendingStudents(id)));
    }

    @PostMapping("/activities")
    public ResponseEntity saveActivity(@RequestBody Activity activity) {
        return ResponseEntity.ok(activityService.saveActivity(activity));
    }

    @PostMapping("activities/comment")
    public ResponseEntity addComment(@RequestBody Comment comment){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.addComment(comment)));
    }

    @PostMapping("/activities/comment/delete/{id}")
    public ResponseEntity deleteComment(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.deleteComment(id)));
    }

    @PostMapping("/activities/{activityId}/approve/{studentId}")
    public ResponseEntity approveStudent(@PathVariable("activityId") Long activityId, @PathVariable("studentId") Long studentId) {
        return ResponseEntity.ok(activityService.approveStudent(activityId, studentId));
    }

    @PostMapping("/activities/{activityId}/reject/{studentId}")
    public ResponseEntity rejectStudent(@PathVariable("activityId") Long activityId, @PathVariable("studentId") Long studentId) {
        log.info("reject");
        return ResponseEntity.ok(activityService.rejectStudent(activityId, studentId));
    }
}
