package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Student extends Person{
    String studentId;
    @ManyToOne
    @ToString.Exclude
    @JsonIgnore
    Teacher teacher;

    @ManyToMany(mappedBy = "enrolledStudent")
    @Builder.Default
    @ToString.Exclude
    List<Activity> enrolledActivities = new ArrayList<>();

    @ManyToMany(mappedBy = "pendingStudents", cascade = CascadeType.ALL)
    @Builder.Default
    @ToString.Exclude
    List<Activity> pendingActivities = new ArrayList<>();

    public List<Activity> getPendingActivities() {
        return pendingActivities;
    }

    public void setPendingActivities(List<Activity> pendingActivities) {
        this.pendingActivities = pendingActivities;
    }

    @Override
    public boolean equals(Object o) {
        Student student = (Student) o;
        // If the object is compared with itself then return true
        if (student.id == this.id) {
            return true;
        } else {
            return false;
        }
    }


}
