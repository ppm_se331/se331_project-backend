package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity

@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Teacher extends Person{
    @OneToMany(mappedBy = "teacher")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Student> advisees = new ArrayList<>();

    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Activity> activities = new ArrayList<>();
}
