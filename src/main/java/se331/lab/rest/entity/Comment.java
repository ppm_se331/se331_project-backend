package se331.lab.rest.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import se331.lab.rest.security.entity.User;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    Long id;
    String text;
    String image;

    @OneToOne
    @JsonBackReference
    @JsonIgnore
    User user;

    @ManyToOne
    @JsonBackReference
    @JsonIgnore
    Activity activity;

    public User getUser(){
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

    @Override
    public boolean equals(Object o){
        Comment comment = (Comment) o;

        if (comment.id == this.id){
            return true;
        } else {
            return false;
        }
    }

}
