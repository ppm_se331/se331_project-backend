package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String activityId;
    String activityName;
    String activityImage;
    Date activityDate;
    String location;
    String description;
    Date regisDateStart;
    Date regisDateEnd;
    Time startTime;

    @ManyToOne
    @JsonIgnore
    Teacher advisor;

    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    @JsonIgnore
    List<Student> enrolledStudent = new ArrayList<>();

    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    @JsonIgnore
    List<Student> pendingStudents = new ArrayList<>();

    @OneToMany(mappedBy = "activity")
    @Builder.Default
    @ToString.Exclude
    @JsonIgnore
    List<Comment> comments = new ArrayList<>();
}
