package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.Userdata;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AdminDto {
    Userdata user;
}
