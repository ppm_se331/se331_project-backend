package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.User;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {
    Long id;
    User user;
    String text;
    String image;
}
