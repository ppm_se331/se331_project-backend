package se331.lab.rest.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.Teacher;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDto {
    Long id;
    String activityId;
    String activityName;
    String activityImage;
    Date activityDate;
    String location;
    String description;
    Date regisDateStart;
    Date regisDateEnd;
    Time startTime;

    TeacherDto advisor;

    @Builder.Default
    List<StudentDto> students = new ArrayList<>();

    @Builder.Default
    List<Student> pendingStudents = new ArrayList<>();

    @Builder.Default
    List<Comment> comments = new ArrayList<>();

}
