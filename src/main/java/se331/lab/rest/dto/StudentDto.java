package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.security.entity.User;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    Long id;
    String studentId;
    String name;
    String surname;
    String image;
    User user;
    List<Activity> enrolledActivities;
    List<Activity> pendingActivities;
}
