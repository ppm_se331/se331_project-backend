package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CommentDao;
import se331.lab.rest.entity.Comment;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
   @Autowired
   CommentDao commentDao;

    @Override
    public List<Comment> getAllComments() {
        return commentDao.getAllComments();
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentDao.saveComment(comment);
    }
}
