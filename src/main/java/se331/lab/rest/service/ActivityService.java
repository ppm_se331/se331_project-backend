package se331.lab.rest.service;

import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.entity.Student;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivities();
    Activity saveActivity(Activity activity);
    Activity findById(Long id);
    Activity addComment(Comment comment);
    Activity deleteComment(Long id);
    Activity approveStudent(Long activityId, Long studentId);
    Activity rejectStudent(Long activityId, Long studentId);
    List<Student> getActivityPendingStudents(Long id);
}
