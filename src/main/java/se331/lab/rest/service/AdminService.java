package se331.lab.rest.service;

import se331.lab.rest.entity.Admin;

import java.util.List;

public interface AdminService {
    List<Admin> getAdmin();

}
