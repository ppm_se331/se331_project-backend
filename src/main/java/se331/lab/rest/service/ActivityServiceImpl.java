package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.ActivityDao;
import se331.lab.rest.dao.CommentDao;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.dao.TeacherDao;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.repository.CommentRepository;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
@Service
@Slf4j
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    ActivityDao activityDao;
    @Autowired
    StudentDao studentDao;
    @Autowired
    TeacherDao teacherDao;
    @Autowired
    CommentDao commentDao;
    @Autowired
    UserRepository userRepository;
    @Autowired
    CommentRepository commentRepository;


    @Override
    public List<Activity> getAllActivities() {
        return activityDao.getAllActivities();
    }

    @Override
    public Activity saveActivity(Activity activity) {
        if(activity.getId() == null){
            return activityDao.saveActivity(activity);
        } else if(activity.getPendingStudents().size() != activityDao.findById(activity.getId()).getPendingStudents().size()){
            Student existStudent = studentDao.findById(activity.getPendingStudents().get(activity.getPendingStudents().size()-1).getId());
            activity.getPendingStudents().remove(existStudent);
            activity.getPendingStudents().add(existStudent);
            studentDao.saveStudent(existStudent);
        }
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity findById(Long id) {
        return activityDao.findById(id);
    }

    @Override
    public Activity addComment(Comment comment) {
        User user;
        Activity activity = activityDao.findById(comment.getActivity().getId());
        Student student = studentDao.findById(comment.getUser().getId());
        Teacher teacher = teacherDao.findById(comment.getUser().getId());
        if(student == null) {
            user = userRepository.findById(teacher.getUser().getId()).orElse(null);
        } else {
            user = userRepository.findById(student.getUser().getId()).orElse(null);
        }
        comment.setActivity(null);
        comment.setUser(null);
        commentDao.saveComment(comment);
        activity.getComments().add(comment);
        comment.setActivity(activity);
        comment.setUser(user);
        commentDao.saveComment(comment);
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity deleteComment(Long id) {
        Comment comment = commentRepository.findById(id).orElse(null);
        Activity activity = activityDao.findById(comment.getActivity().getId());
        activity.getComments().remove(comment);
        comment.setActivity(null);
        commentRepository.deleteById(comment.getId());
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity approveStudent(Long activityId, Long studentId) {
        log.info("accept student");
        Activity activity = activityDao.findById(activityId);
        Student student = null;
        for (Student s: activity.getPendingStudents()) {
            if(s.getId() == studentId){
                student = s;
            }
        }
        activity.getPendingStudents().remove(student);
        activity.getEnrolledStudent().add(student);
        studentDao.saveStudent(student);
        return activityDao.saveActivity(activity);    }

    @Override
    public Activity rejectStudent(Long activityId, Long studentId) {
        log.info("reject student");
        Activity activity = activityDao.findById(activityId);
        Student student = studentDao.findById(studentId);
        activity.getPendingStudents().remove(student);
        studentDao.saveStudent(student);
        return activityDao.saveActivity(activity);
    }

    @Override
    public List<Student> getActivityPendingStudents(Long id) {
        Activity activity = activityDao.findById(id);
        List<Student> students = new ArrayList<>();
        if(activity.getPendingStudents() != null) {
            students.addAll(activity.getPendingStudents());
        }
        return students;
    }
}
