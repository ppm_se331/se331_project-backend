package se331.lab.rest.service;

import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();
    Student findById(Long studentId);
    Student saveStudent(Student student);
    List<Activity> getStudentActivities(Long id);
    List<Student> getWaitingStudents();
    Student approveStudent(Long id);
    void rejectStudent(Long id);
}
