package se331.lab.rest.service;

import se331.lab.rest.entity.Comment;

import java.util.List;

public interface CommentService {
    List<Comment> getAllComments();
    Comment saveComment(Comment comment);

}
