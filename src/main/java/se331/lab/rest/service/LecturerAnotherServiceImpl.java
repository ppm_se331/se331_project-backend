package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.LecturerAnotherDao;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class LecturerAnotherServiceImpl implements LecturerAnotherService {
    @Autowired
    LecturerAnotherDao lecturerAnotherDao;
    @Override
    public Teacher getLecturerByLastName(String lastname) {
        return lecturerAnotherDao.getLecturerByLastName(lastname);
    }


}
