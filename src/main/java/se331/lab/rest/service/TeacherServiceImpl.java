package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.TeacherDao;
import se331.lab.rest.entity.Teacher;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    TeacherDao lectureDao;
    @Override
    public List<Teacher> getAllLecturers() {
        return lectureDao.getAllTeachers();
    }
}
