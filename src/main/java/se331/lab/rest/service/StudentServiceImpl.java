package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.StudentRepository;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.UserRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentDao studentDao;
    @Autowired
    UserRepository userRepository;
    @Autowired
    StudentRepository studentRepository;

    @Override
    public List<Student> getAllStudent() {
        log.info("service received called");
        List<Student> students = studentDao.getAllStudent();
        log.info("service received {} \n from dao", students);
        return students;
    }

    @Override
    public Student findById(Long id) {
        return studentDao.findById(id);
    }

    @Override
    public Student saveStudent(Student student) {
        log.info("pass service");
        if(student.getId() == null) {
            Authority auth = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            student.getUser().setPassword(encoder.encode(student.getUser().getPassword()));
            student.getUser().setLastPasswordResetDate(Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()));
            student.getUser().getAuthorities().add(auth);
            userRepository.save(student.getUser());
        } else {
            User user = studentDao.findById(student.getId()).getUser();
            user.setFirstname(student.getName());
            user.setLastname(student.getSurname());
            user.setAppUser(student);
            userRepository.save(user);
            student.setUser(user);
        }

        return studentDao.saveStudent(student);
    }

    @Override
    public List<Activity> getStudentActivities(Long id) {
        List<Activity> activities = new ArrayList<>();
        Student student = studentDao.findById(id);
        for (Activity a: student.getEnrolledActivities()) {
            activities.add(a);
        }
        for (Activity a: student.getPendingActivities()) {
            activities.add(a);
        }
        return activities;
    }

    @Override
    public List<Student> getWaitingStudents() {
        List<Student> students = new ArrayList<>();
        List<Student> dbStudent = studentRepository.findAll();
        for (Student student: dbStudent) {
            if(Boolean.compare(student.getUser().getEnabled(), false) == 0 && student.getUser() != null) {
                students.add(student);
            }
        }
        return students;
    }

    @Override
    public Student approveStudent(Long id) {
        Student student = studentDao.findById(id);
        student.getUser().setEnabled(true);
        return studentDao.saveStudent(student);
    }

    @Override
    public void rejectStudent(Long id) {
        User user = userRepository.findById(studentDao.findById(id).getUser().getId()).orElse(null);
        Long userId = studentDao.findById(id).getUser().getId();
        Student student = studentDao.findById(id);
        student.setUser(null);
        user.setAppUser(null);
        studentDao.saveStudent(student);
        if(studentDao.findById(id) != null){
            studentRepository.deleteById(id);
        }
    }
}
