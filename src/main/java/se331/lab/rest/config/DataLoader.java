package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.*;
import se331.lab.rest.repository.*;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.ZoneId;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    TeacherRepository teacherRepository;
    @Autowired
    ActivityRepository ActivityRepository;
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    AuthorityRepository authorityRepository;
    @Autowired
    UserRepository userRepository;
    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        //Create Student Obj
        Student student1 = Student.builder()
                .studentId("SE-001")
                .name("Jiramed")
                .surname("The minister")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/gu.jpg?alt=media&token=1af4e004-bb87-42ff-8440-256c60329952")
                .dob(Date.valueOf("1998-11-07"))
                .build();
        Student student2 = Student.builder()
                .studentId("SE-002")
                .name("Chartchai")
                .surname("BNK48")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/mek.jpg?alt=media&token=eb4900cd-cc40-49c3-b9fc-9e171f631714")
                .dob(Date.valueOf("1998-11-12"))
                .build();
        Student student3 = Student.builder()
                .studentId("SE-003")
                .name("Pongpanot")
                .surname("Nobita")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/p'plurm.jpg?alt=media&token=0543e7c9-f463-4f16-b109-e625463e32e7")
                .dob(Date.valueOf("1998-11-12"))
                .build();
        Student student4 = Student.builder()
                .studentId("SE-004")
                .name("Jurgen")
                .surname("Klopp")
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/kloop.png?alt=media&token=eda404ff-b854-43ed-96ee-92f26cdd4380")
                .dob(Date.valueOf("1998-11-11"))
                .build();
        Student student5 = Student.builder()
                .studentId("SE-005")
                .name("Mohamed")
                .surname("Salah")
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/salah.jpg?alt=media&token=a2f1f204-f17f-443e-b280-0c06e184a99a")
                .dob(Date.valueOf("1998-11-11"))
                .build();


        student1 = studentRepository.save(student1);
        student2 = studentRepository.save(student2);
        student3 = studentRepository.save(student3);
        student4 = studentRepository.save(student4);
        student5 = studentRepository.save(student5);

        //Create Teacher Obj
        Teacher teacher1 = Teacher.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Teacher%2Fchartchai.jpg?alt=media&token=cb0b6db5-673c-4991-9759-0d01a3d16b66")
                .build();
        Teacher teacher2 = Teacher.builder()
                .name("Passakorn")
                .surname("Phannachitta")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Teacher%2Fpassakorn.jpg?alt=media&token=77087827-5f86-417f-810c-2a8a046041b4")
                .build();
        Teacher teacher3 = Teacher.builder()
                .name("Prompong")
                .surname("Sugunnasi")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Teacher%2Fprompong.jpg?alt=media&token=3f79a09a-0752-4b76-b0bc-ae8344ae514b")
                .build();

        teacher1 = this.teacherRepository.save(teacher1);
        teacher2 = this.teacherRepository.save(teacher2);
        teacher3 = this.teacherRepository.save(teacher3);

        //Create Activities Obj
        Activity activity1  = Activity.builder()
                .activityId("001")
                .activityName("Trekking")
                .activityImage("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Activity%2FTrekking.jpg?alt=media&token=5979229a-83d6-4308-91b2-8b63bc6ae6c3")
                .activityDate(Date.valueOf("2019-09-14"))
                .location("CAMT - DoiSutep")
                .description("Terkking tried together")
                .regisDateStart(Date.valueOf("2019-08-01"))
                .regisDateEnd(Date.valueOf("2019-08-30"))
                .startTime(Time.valueOf("4:00:00"))
                .build();
        Activity activity2 = Activity.builder()
                .activityId("002")
                .activityName("SE Show Pro")
                .activityImage("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Activity%2FShowpro.jpg?alt=media&token=ce68b9d1-2db8-492b-95db-5a400eed4d6b")
                .activityDate(Date.valueOf("2020-01-30"))
                .location("CAMT")
                .description("XXX fair to represent our xxx projects")
                .regisDateStart(Date.valueOf("2019-11-06"))
                .regisDateEnd(Date.valueOf("2019-12-30"))
                .startTime(Time.valueOf("7:00:00"))
                .build();
        Activity activity3 = Activity.builder()
                .activityId("003")
                .activityName("Songkran")
                .activityImage("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Activity%2FSongkran.jpg?alt=media&token=596c70fb-9e71-43da-8634-081a208e803e")
                .activityDate(Date.valueOf("2019-04-13"))
                .location("CAMT - Walking Street")
                .description("You are already wet")
                .regisDateStart(Date.valueOf("2019-04-10"))
                .regisDateEnd(Date.valueOf("2019-04-12"))
                .startTime(Time.valueOf("7:30:00"))
                .build();
        Activity activity4 = Activity.builder()
                .activityId("004")
                .activityName("Khantok CAMT")
                .activityImage("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/Activity%2FKhantoke.jpg?alt=media&token=6c1c53f1-0f68-48cd-b893-c31051cbc93f")
                .activityDate(Date.valueOf("2019-09-20"))
                .location("CAMT")
                .description("Free Lanna Dinner")
                .regisDateStart(Date.valueOf("2019-09-11"))
                .regisDateEnd(Date.valueOf("2019-09-20"))
                .startTime(Time.valueOf("17:00:00"))
                .build();

        activity1 = this.ActivityRepository.save(activity1);
        activity2 = this.ActivityRepository.save(activity2);
        activity3 = this.ActivityRepository.save(activity3);
        activity4 = this.ActivityRepository.save(activity4);

        //Create Admin Obj
        Admin admin = Admin.builder()
                .build();
        admin = this.adminRepository.save(admin);

        //Create Comment Obj
        Comment comment1 = Comment.builder()
                .text("Aj.Dto so HANDSOME!!!")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2F57012667_327442531249981_7432154422819422208_o.jpg?alt=media&token=401903aa-1330-4b12-9536-282323daa66f")
                .build();
        Comment comment2 = Comment.builder()
                .text("Rate my sexy post.")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2F56806569_327464084581159_1586428750113800192_o.jpg?alt=media&token=ae95a563-5d10-4874-92d9-b21af10bbb1a")
                .build();
        Comment comment3 = Comment.builder()
                .text("This artwork is fecking cool. Who do this!!")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2FCoverFront.png?alt=media&token=59e6d682-5bfa-477e-ad99-73322a321cfd")
                .build();
        Comment comment4 = Comment.builder()
                .text("I always smile when see you face.")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2F_EVL3839.jpg?alt=media&token=fecfb96a-33b6-4874-b9e5-31b94abcbb1c")
                .build();
        Comment comment5 = Comment.builder()
                .text("This is the best ever activity in ma life.")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2F%E0%B8%94%E0%B8%AD%E0%B8%A2.jpg?alt=media&token=1d6fb6ce-2cb2-4bfb-9a41-997a20089ac4")
                .build();
        Comment comment6 = Comment.builder()
                .text("Dang-it!! Why northern food is very delicious like heaven of food.")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-e68e1.appspot.com/o/%E0%B9%81%E0%B8%99%E0%B8%97%E0%B8%97%E0%B8%B3%E0%B8%B7%E0%B8%B0%2F%E0%B9%82%E0%B8%95%E0%B8%81.png?alt=media&token=faa8f58b-c3dc-477b-a759-74f36506bcc5")
                .build();
        Comment comment7 = Comment.builder()
                .text("Song Tee Dee is Song Pra .....")
                .image(null)
                .build();
        Comment comment8 = Comment.builder()
                .text("We will vote Aj.Dto to be prime minister of thailand")
                .image(null)
                .build();

        comment1= this.commentRepository.save(comment1);
        comment2= this.commentRepository.save(comment2);
        comment3= this.commentRepository.save(comment3);
        comment4= this.commentRepository.save(comment4);
        comment5= this.commentRepository.save(comment5);
        comment6= this.commentRepository.save(comment6);
        comment7= this.commentRepository.save(comment7);
        comment8= this.commentRepository.save(comment8);


        //Set obj info
        teacher1.getAdvisees().add(student1);
        student1.setTeacher(teacher1);
        teacher1.getAdvisees().add(student2);
        student2.setTeacher(teacher1);
        teacher2.getAdvisees().add(student3);
        student3.setTeacher(teacher2);
        teacher3.getAdvisees().add(student4);
        student4.setTeacher(teacher3);
        teacher3.getAdvisees().add(student5);
        student5.setTeacher(teacher3);

        teacher1.getActivities().add(activity1);
        activity1.setAdvisor(teacher1);
        teacher2.getActivities().add(activity2);
        activity2.setAdvisor(teacher2);
        teacher2.getActivities().add(activity3);
        activity3.setAdvisor(teacher2);
        teacher3.getActivities().add(activity4);
        activity3.setAdvisor(teacher3);

        student1.getEnrolledActivities().add(activity1);
        activity1.getEnrolledStudent().add(student1);
        student1.getEnrolledActivities().add(activity2);
        activity2.getEnrolledStudent().add(student1);
        student2.getEnrolledActivities().add(activity1);
        activity1.getEnrolledStudent().add(student2);

        student3.getEnrolledActivities().add(activity2);
        activity2.getEnrolledStudent().add(student3);
        activity3.getEnrolledStudent().add(student4);
        activity3.getEnrolledStudent().add(student5);
        activity3.getEnrolledStudent().add(student1);
        student1.getEnrolledActivities().add(activity3);
        student4.getEnrolledActivities().add(activity3);
        student5.getEnrolledActivities().add(activity3);

        activity4.getEnrolledStudent().add(student1);
        activity4.getEnrolledStudent().add(student2);
        activity4.getEnrolledStudent().add(student3);
        activity4.getEnrolledStudent().add(student4);
        activity4.getEnrolledStudent().add(student5);
        student1.getEnrolledActivities().add(activity4);
        student2.getEnrolledActivities().add(activity4);
        student3.getEnrolledActivities().add(activity4);
        student4.getEnrolledActivities().add(activity4);
        student5.getEnrolledActivities().add(activity4);


        activity1.setAdvisor(teacher2);
        teacher2.getActivities().add(activity1);
        activity2.setAdvisor(teacher1);
        teacher1.getActivities().add(activity2);
        activity3.setAdvisor(teacher3);
        teacher3.getActivities().add(activity3);
        activity4.setAdvisor(teacher1);
        teacher1.getActivities().add(activity4);


        //Set account
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        Authority auth1 = Authority.builder().name(AuthorityName.ROLE_TEACHER).build();
        Authority auth2 = Authority.builder().name(AuthorityName.ROLE_STUDENT).build();
        Authority auth3 = Authority.builder().name(AuthorityName.ROLE_ADMIN).build();
        User user1, user2, user3, user4,user5,user6,user7,user8;
        user1 = User.builder()
                .username("ajdto@gmail.com")
                .password(encoder.encode("ajdto"))
                .firstname("Chartchai")
                .lastname("Doungsa-ard")
                .email("techer@teacher.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user2 = User.builder()
                .username("jiramed@gmail.com")
                .password(encoder.encode("jiramed"))
                .firstname("Jiramed")
                .lastname("Wiwiwiwiwi")
                .email("jiramed@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user3 = User.builder()
                .username("Tanksin@gmail.com")
                .password(encoder.encode("Tanksin"))
                .firstname("Tanksin")
                .lastname("Sinnawun")
                .email("Tanksin@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user4 = User.builder()
                .username("chutchai@gmail.com")
                .password(encoder.encode("chutchai"))
                .firstname("chutchai")
                .lastname("PeeMak")
                .email("chutchai@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user5 = User.builder()
                .username("pplurm@gmail.com")
                .password(encoder.encode("pplurm"))
                .firstname("pplurm")
                .lastname("Manyworks")
                .email("pplurm@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user6 = User.builder()
                .username("surprise@gmail.com")
                .password(encoder.encode("surprise"))
                .firstname("surprise")
                .lastname("MotherTrucker")
                .email("surprise@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user7 = User.builder()
                .username("somefries@gmail.com")
                .password(encoder.encode("somefries"))
                .firstname("somefries")
                .lastname("MotherTrucker")
                .email("somefries@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        user8 = User.builder()
                .username("eatrice@gmail.com")
                .password(encoder.encode("eatrice"))
                .firstname("eatrice")
                .lastname("MotherTrucker")
                .email("eatrice@gmail.com")
                .enabled(true)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019,01,
                        01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();

        //Save Role
        authorityRepository.save(auth1);
        authorityRepository.save(auth2);
        authorityRepository.save(auth3);

        user1.getAuthorities().add(auth1);
        user2.getAuthorities().add(auth2);
        user3.getAuthorities().add(auth3);
        user4.getAuthorities().add(auth2);
        user5.getAuthorities().add(auth2);
        user6.getAuthorities().add(auth2);
        user7.getAuthorities().add(auth2);
        user8.getAuthorities().add(auth2);

        //Save user
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        userRepository.save(user5);
        userRepository.save(user6);
        userRepository.save(user7);
        userRepository.save(user8);

        //Set who comment
        comment1.setUser(user2);
        comment2.setUser(user4);
        comment3.setUser(user8);
        comment4.setUser(user6);
        comment5.setUser(user5);
        comment6.setUser(user7);
        comment7.setUser(user8);
        comment8.setUser(user8);

        //Set comment in whice activity
        comment1.setActivity(activity3);
        comment2.setActivity(activity3);
        comment3.setActivity(activity2);
        comment4.setActivity(activity1);
        comment5.setActivity(activity1);
        comment6.setActivity(activity4);
        comment7.setActivity(activity3);
        comment8.setActivity(activity3);

        //Get comment to act
        activity3.getComments().add(comment1);
        activity3.getComments().add(comment2);
        activity2.getComments().add(comment3);
        activity1.getComments().add(comment4);
        activity1.getComments().add(comment5);
        activity4.getComments().add(comment6);
        activity3.getComments().add(comment7);
        activity3.getComments().add(comment8);

        teacher1.setUser(user1);
        user1.setAppUser(teacher1);
        student1.setUser(user2);
        user2.setAppUser(student1);
        admin.setUser(user3);
        user3.setAppUser(admin);
        student2.setUser(user4);
        user4.setAppUser(student2);
        student3.setUser(user5);
        user5.setAppUser(student3);
    }


}

