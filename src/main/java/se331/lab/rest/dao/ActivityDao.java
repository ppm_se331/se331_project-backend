package se331.lab.rest.dao;

import se331.lab.rest.entity.Activity;

import java.util.List;

public interface ActivityDao {
    List<Activity> getAllActivities();
    Activity saveActivity(Activity activity);
    Activity findById(Long id);
}
