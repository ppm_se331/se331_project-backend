package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.repository.CommentRepository;
import se331.lab.rest.service.CommentService;

import java.util.List;

@Repository
public class CommentDaoImpl implements CommentDao{
    @Autowired
    CommentRepository commentRepository;

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentRepository.save(comment);
    }
}
