package se331.lab.rest.dao;

import se331.lab.rest.entity.Teacher;

import java.util.List;

public interface LecturerAnotherDao {
    Teacher getLecturerByLastName(String lastName);
    List<Teacher> getAllLecturer();

}
