package se331.lab.rest.dao;

import se331.lab.rest.entity.Comment;

import java.util.List;

public interface CommentDao {
    List<Comment> getAllComments();
    Comment saveComment(Comment comment);
}
