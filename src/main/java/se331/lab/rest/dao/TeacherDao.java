package se331.lab.rest.dao;

import se331.lab.rest.entity.Teacher;

import java.util.List;

public interface TeacherDao {
    List<Teacher> getAllTeachers();
    Teacher saveTeacher(Teacher teacher);
    Teacher findById(Long id);
}
