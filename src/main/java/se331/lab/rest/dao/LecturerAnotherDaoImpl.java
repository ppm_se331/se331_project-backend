package se331.lab.rest.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.repository.TeacherRepository;

import java.util.List;

@Repository
public class LecturerAnotherDaoImpl implements LecturerAnotherDao {
    @Autowired
    TeacherRepository teacherRepository;
    @Override
    public Teacher getLecturerByLastName(String lastName) {
        return teacherRepository.findBySurname(lastName);
    }

    @Override
    public List<Teacher> getAllLecturer() {
        return teacherRepository.findAll();
    }
}
