package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Profile("LabDao")
@Repository
@Slf4j
public class StudentDaoImpl implements StudentDao{
    List<Student> students;


    @Override
    public List<Student> getAllStudent() {
        log.info("LabDao is called");
        return students;
    }

    @Override
    public Student findById(Long studentId) {
        return students.get((int) (studentId -1));
    }

    @Override
    public Student saveStudent(Student student) {
        student.setId((long) students.size());
        students.add(student);
        return student;
    }
}
